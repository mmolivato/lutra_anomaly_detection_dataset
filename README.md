# LUTRA Anomaly Classification Dataset
This is the dataset made to classify anomalies in LUTRA boats behaviour. \
This boat is used as Autonomous Surface Vessel in the [INTCATCH project](https://www.intcatch.eu/) for Horizon 2020 for autonomous water monitoring with low cost and low mantainence devices.
For more details about LUTRA boats you can go to [LUTRA Airboat Series](http://senseplatypus.com/lutra-airboat) link.

![LUTRA_img](https://images.squarespace-cdn.com/content/v1/5556059de4b081dbcc1dfcf6/1439711046599-HM49ZQY88NIQTLQE7BFY/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0pE4cef1KNtWo36k-CFnr6wOF2g5O-PFkVuvW_ba6dQUZZpzEt6WQHHQe4EHY-NJIA/image-asset.png?format=500w)


The system is composed as showed in the following image:

![Global_LUTRA_Schema](figures/global_schema.jpg)

A result of a **water_oxygen** scanning of a piece of lake could be seen in the next figure:

![LUTRA_scan](https://images.squarespace-cdn.com/content/v1/5556059de4b081dbcc1dfcf6/1439715248739-VXCGA210YNFKHW8V6NRK/ke17ZwdGBToddI8pDm48kAHcQ-HhqLIInw3J2rGyaKVZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIYtMyuax29lnksVEW-3bTB1L5uExXNspJT780HVAtAQU/image-asset.png?format=500w)

## Dataset creation

The dataset was created performing simple system attacks and simulating system faults on the LUTRA boat.
- The **Attacks** classes are:
    - Network attacks:
        - **dos_0**: we wanted to simulate a Denial of Service attack on the UDP protocol between the Android Server on the boat and an Android Client. The payload size of the packets used for the DoS attack was 0. (0 bytes payload size)
        - **dos_32**: similar to the previous one but using 32 byte random value as payload.
    - Physical attack:
        - **stolen**: we wanted to simulate a thief who steals the boat and runs away.
- The **Faults** classes are:
    - GPS fault: 
        - **fault_gpsdown**: we wanted to simulate a gps down fault in which the gps sensor is broken or powered off.
    - The boat got stuck somewhere:
        - **fault_stucked**: (the name comes from a *wrong* renaming procedure: got stuck -> stucked) we wanted to simulate when the boat got stuck (*stucked*) cause vegetation or garbage. 

## Dataset sample format
The single sample of the dataset contains:
- Android sensors
- Dump of important variables for the server application

The whole sample is a record of the following variables:

    accelerometer                  float64
    accelerometer_uncalibrated     float64
    device_orientation             float64
    game_rotation_vector           float64
    geomagnetic_rotation_vector    float64
    gps_accuracy                   float64
    gps_altitude                   float64
    gps_bearing                    float64
    gps_latitude                   float64
    gps_longitude                  float64
    gps_speed                      float64
    gps_time                         int64
    gravity                        float64
    gyroscope                      float64
    gyroscope_uncalibrated         float64
    label                           string
    light                          float64
    linear_acceleration            float64
    magnetic_field                 float64
    magnetic_field_uncalibrated    float64
    motor_vel_0                    float64
    motor_vel_1                    float64
    net_rec_address                 string
    net_rec_length                   int64
    net_rec_malformed                int64
    net_rec_offset                   int64
    net_rec_payload_encb64          string
    net_rec_payload_plainb64        string
    net_rec_port                     int64
    net_rec_tstamp                   int64
    net_send_address                string
    net_send_length                  int64
    net_send_payload_encb64         string
    net_send_payload_plainb64       string
    net_send_tstamp                  int64
    orientation                    float64
    pressure                       float64
    proximity                      float64
    rotation_vector                float64
    sensor                         float64
    step_counter                   float64
    step_detector                  float64
    tilt_detector                  float64
    water_oxygen                   float64
    

## Most relevant feature-class correlation
---
- Class **dos_0**:

    - Highest *mean* and *std* for features:
        - net_rec_offset
        - net_send_length
    - High *mean* and *std* for features:
        - accelerometer
        - gps_speed
        - gravity
        - gyroscope
        - linear_acceleration
        - pressure
    - Low *mean* and *std* for features: 
        - net_rec_port
    - Lowest *mean* and *std* for features: 
        - net_rec_length

- Class **dos_32**:

    - High *mean* and *std* for features:
        - net_rec_offset
    - Low *mean* and *std* for features: 
        - net_rec_port
    - Lowest *mean* and *std* for features: 
        - net_send_length

---
- Class **fault_gpsdown**:

    - Highest *mean* and *std* for features: 
        - geomagnetic_rotation_vector
    - High *mean* and *std* for features: 
        - accelerometer
        - gravity
        - gyroscope
        - linear_acceleration
        - pressure
        - rotation_vector
        - sensor
    - Low *mean* and *std* for features: 
        - net_rec_offset
    - Lowest *mean* and *std* for features: 
        - gps_accuracy
        - gps_altitude
        - gps_bearing
        - gps_latitude
        - gps_longitude
        - gps_speed

---
- Class **fault_stucked**:

    - Highest *mean* and *std* for features: 
        - gps_accuracy
        - magnetic_field
    - High *mean* and *std* for features: 
        - net_send_length
    - Lowest *std* for features: 
        - game_rotation_vector
        - geomagnetic_rotation_vector
        - motor_vel_0
        - motor_vel_1
        - rotation_vector
        - water_oxygen
    - Lowest *mean* and *std* for features: 
        - gps_bearing
        - gps_speed
        - gyroscope
        - light
        - linear_acceleration
        - orientation
        - step_counter

---
- Class **stolen**:

    - Highest *mean* and *std* for features: 
        - accelerometer
        - game_rotation_vector
        - gps_latitude
        - gps_longitude
        - gps_speed
        - gravity
        - gyroscope
        - linear_acceleration
        - orientation
        - pressure
        - rotation_vector
        - sensor
        - step_counter
        - water_oxygen
    - High *mean* and *std* for features: 
        - geomagnetic_rotation_vector
    - Lowest *mean* and *std* for features: 
        - net_rec_offset

---
The previous consideration are retrieved by the following plots:

![Standardized Mean Correlation plot](plots/standardized_mean_correlation.png)

![Standardized Standard Deviation Correlation plot](plots/standardized_standard_deviation_correlation.png)

For more detailed plots on features mean and standard deviantion you can watch plots into **_plots/_** folder.

## Dataset utilization

The dataset could be easily imported with pandas using python code:

```python
import pandas as pd

lutra_dataset = pd.read_csv('new_datasets/lutra_dataset_clean.csv')

print(lutra_dataset)
```
